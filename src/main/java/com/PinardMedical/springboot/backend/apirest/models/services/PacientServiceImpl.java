package com.PinardMedical.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.PinardMedical.springboot.backend.apirest.models.DAO.IPacientDao;
import com.PinardMedical.springboot.backend.apirest.models.entity.Pacient;

@Service 
public class PacientServiceImpl implements IPacientService{

	@Autowired
	private IPacientDao pacientDao;
	@Override
	@Transactional(readOnly = true)
	public List<Pacient> findAll() {
		return (List<Pacient>) pacientDao.findAll();
	}
	@Override
	public Pacient save(Pacient pacient) {
		// TODO Auto-generated method stub
		return pacientDao.save(pacient);
	}
	@Override
	public Pacient findById(Long id) {
		// TODO Auto-generated method stub
		return pacientDao.findById(id).orElse(null);
	}
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		pacientDao.deleteById(id);
	}

}
