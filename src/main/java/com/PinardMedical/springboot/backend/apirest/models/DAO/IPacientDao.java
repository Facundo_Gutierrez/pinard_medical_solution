package com.PinardMedical.springboot.backend.apirest.models.DAO;

import org.springframework.data.repository.CrudRepository;

import com.PinardMedical.springboot.backend.apirest.models.entity.Pacient;

public interface IPacientDao extends CrudRepository<Pacient, Long> {

}
