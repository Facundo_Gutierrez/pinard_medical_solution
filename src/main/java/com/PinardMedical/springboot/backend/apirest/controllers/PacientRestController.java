package com.PinardMedical.springboot.backend.apirest.controllers;

import java.util.List;

import javax.websocket.ClientEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.PinardMedical.springboot.backend.apirest.models.entity.Pacient;
import com.PinardMedical.springboot.backend.apirest.models.services.IPacientService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PacientRestController {

	@Autowired
	private IPacientService pacientService;
	
	@GetMapping("/pacient")
	public List<Pacient> index(){
		return pacientService.findAll();
		
	}
	
	@GetMapping("/pacient/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Pacient show(@PathVariable Long id) {
		return pacientService.findById(id);
	}
	
	@PostMapping("/pacient")
	@ResponseStatus(HttpStatus.CREATED)
	public Pacient create(@RequestBody Pacient pacient) {
		return pacientService.save(pacient);
	} 
	
	@PutMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Pacient update(@RequestBody Pacient pacient, @PathVariable Long id){
		Pacient pacientUpdate = pacientService.findById(id);
		
		pacientUpdate.setApellido(pacient.getApellido());
		pacientUpdate.setNombre(pacient.getNombre());
		pacientUpdate.setEmail(pacient.getEmail());
		
		return pacientService.save(pacientUpdate);
		
	}
	
	@DeleteMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		pacientService.delete(id);
	}
}
