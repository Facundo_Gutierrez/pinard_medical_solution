package com.PinardMedical.springboot.backend.apirest.models.services;

import java.util.List;

import com.PinardMedical.springboot.backend.apirest.models.entity.Pacient;

public interface IPacientService {
	
	public List<Pacient> findAll();
	
	public Pacient save(Pacient pacient);
	
	public Pacient findById(Long id);
	
	public void delete (Long id);
	

}
