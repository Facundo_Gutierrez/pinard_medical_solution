package com.PinardMedical.springboot.backend.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBackendPinarMedicalApirest2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBackendPinarMedicalApirest2Application.class, args);
	}

}
